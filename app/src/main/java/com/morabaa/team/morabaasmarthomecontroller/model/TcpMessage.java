package com.morabaa.team.morabaasmarthomecontroller.model;

import com.morabaa.team.morabaasmarthomecontroller.utils.SV;
import java.util.Date;

/**
 * Created by eagle on 3/1/2018.
 */

public class TcpMessage {
      
      private String command;
      private String bodyClass;
      private String body;
      private String DeviceId = SV.DEVICE_ID;
      
      public TcpMessage() {
      }
      
      
      public String getCommand() {
            return command;
      }
      
      public void setCommand(String command) {
            this.command = command;
      }
      
      public String getBodyClass() {
            return bodyClass;
      }
      
      public void setBodyClass(String bodyClass) {
            this.bodyClass = bodyClass;
      }
      
      public String getBody() {
            return body;
      }
      
      public void setBody(String body) {
            this.body = body;
      }
      
      public String getDeviceId() {
            return DeviceId;
      }
      
      public void setDeviceId(String deviceId) {
            DeviceId = deviceId;
      }
      
      @Override
      public String toString() {
            return "**************************" +
                  new Date() + ": " +
                  "\nCommand\t" +
                  this.getCommand() + "\t" +
                  "BodyClass\t" + this.getBodyClass() + "\n" +
                  "Body\t" + this.getBody() + "\n" +
                  "DeviceId\t" + this.getDeviceId() +
                  "\n**************************\n";
      }
}
