package com.morabaa.team.morabaasmarthomecontroller.model;

import java.util.Date;

/**
 * Created by eagle on 2/28/2018.
 */

public class UARTMessage {
      
      private String cmd;
      private long controllerAddress;
      private long deviceAddress;
      private int state;
      
      public UARTMessage() {
      }
      
      public String getCmd() {
            return cmd;
      }
      
      public void setCmd(String cmd) {
            this.cmd = cmd;
      }
      
      public long getControllerAddress() {
            return controllerAddress;
      }
      
      public void setControllerAddress(long controllerAddress) {
            this.controllerAddress = controllerAddress;
      }
      
      public long getDeviceAddress() {
            return deviceAddress;
      }
      
      public void setDeviceAddress(long deviceAddress) {
            this.deviceAddress = deviceAddress;
      }
      
      public int getState() {
            return state;
      }
      
      public void setState(int state) {
            this.state = state;
      }
      
      @Override
      public String toString() {
            return "**************************" +
                  new Date() + ": " +
                  "\nCmd\t" + this.getCmd() + "\t" +
                  "ControllerAddress\t" + this.getControllerAddress() + "\t" +
                  "DeviceAddress \t" + this.getDeviceAddress() + "\t" +
                  "State\t" + this.getState() +
                  "\n**************************\n";
      }
}
