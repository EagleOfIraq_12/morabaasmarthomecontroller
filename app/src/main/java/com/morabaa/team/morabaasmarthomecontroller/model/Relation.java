package com.morabaa.team.morabaasmarthomecontroller.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
public class Relation {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private long deviceId;
    private long toConnectDeviceId;
    private int relationType;
    private int relationValue;

    public Relation() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    public long getToConnectDeviceId() {
        return toConnectDeviceId;
    }

    public void setToConnectDeviceId(long toConnectDeviceId) {
        this.toConnectDeviceId = toConnectDeviceId;
    }

    public int getRelationType() {
        return relationType;
    }

    public void setRelationType(int relationType) {
        this.relationType = relationType;
    }

    public int getRelationValue() {
        return relationValue;
    }

    public void setRelationValue(int relationValue) {
        this.relationValue = relationValue;
    }

    public static class TYPE {
        public static final int WITH = 1;
        public static final int OPPOSITE = 2;
        public static final int TURN_ON_WITH = 3;
        public static final int TURN_ON_OPPOSITE = 4;


        public static Map<Integer, String> getTypes() {
            Map<Integer, String> types = new HashMap<>();
            types.put(WITH, " مع");
            types.put(OPPOSITE, "عكس");
            types.put(TURN_ON_WITH, "تشغيل مع");
            types.put(TURN_ON_OPPOSITE, "تشغيل عكس");
            return types;
        }

        public static List<String> getNames() {
            return new ArrayList<>(getTypes().values());
        }

        public static String getNameById(int id) {
            String name = "";
            for (int i : getTypes().keySet()) {
                if (id == i) {
                    return getTypes().get(id);
                }
            }
            return name;
        }

        public static int getIdByName(String name) {
            int id = 0;
            for (int i : getTypes().keySet()) {
                if (getTypes().get(i).equals(name)) {
                    return i;
                }
            }
            return id;
        }
    }

    @Dao
    public interface RelationDuo {

        @Query("SELECT * FROM Relation")
        List<Relation> RELATIONS();


        @Query("DELETE FROM Relation")
        void deleteAll();

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(Relation... relations);

        @Query("SELECT * FROM Relation WHERE deviceId = :deviceId")
        List<Relation> getRelationsByDeviceId(long deviceId);

        @Delete
        void delete(Relation... relations);

        @Update
        void Update(Relation... relations);

        @Query("SELECT COUNT(id) FROM Relation")
        int count();
    }
}
