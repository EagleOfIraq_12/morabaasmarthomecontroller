package com.morabaa.team.morabaasmarthomecontroller.services;

import static com.morabaa.team.morabaasmarthomecontroller.MainActivity.IPS;
import static com.morabaa.team.morabaasmarthomecontroller.MainActivity.db;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;

import com.google.gson.Gson;
import com.morabaa.team.morabaasmarthomecontroller.MainActivity;
import com.morabaa.team.morabaasmarthomecontroller.model.Device;
import com.morabaa.team.morabaasmarthomecontroller.model.Room;
import com.morabaa.team.morabaasmarthomecontroller.model.TcpMessage;
import com.morabaa.team.morabaasmarthomecontroller.utils.SV.TCP_COMMANDS;
import com.morabaa.team.morabaasmarthomecontroller.utils.SV.TCP_COMMANDS.OTHER;
import com.morabaa.team.morabaasmarthomecontroller.utils.SV.TCP_COMMANDS.ROOM;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;

/**
 * Created by eagle on 2/24/2018.
 */

public class TcpServerService extends Service {

    public static TcpServerService instant;
    TcpCallback tcpCallback;

    public static TcpServerService getInstant() {
        return instant;
    }

    public static void setInstant(TcpServerService instant) {
        TcpServerService.instant = instant;
    }

    public static String getLocalIpAddress() {
        final WifiManager wifiManager = (WifiManager) MainActivity.ctx
                .getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        final ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        byteBuffer.putInt(wifiInfo.getIpAddress());
        try {
            final InetAddress inetAddress = InetAddress
                    .getByAddress(null, byteBuffer.array());
            return inetAddress.getHostAddress();
        } catch (UnknownHostException e) {
        }
        return null;
    }

    public TcpCallback getTcpCallback() {
        return tcpCallback;
    }

    public void setTcpCallback(
            TcpCallback tcpCallback) {
//            this.tcpCallback = tcpCallback;
        this.tcpCallback = MainActivity.instance;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("TcpServerService Started at :\t" + new Date());
        instant = this;
        setTcpCallback(null);

        run();
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void run() {
        new Thread(() -> {
            try {
                System.out.println("Starting socket thread...");
                ServerSocket serverSocket = new ServerSocket(21111);
                System.out
                        .println(
                                "ServerSocket created, waiting for incoming connections......");
                while (true) {
                    final Socket socket = serverSocket.accept();
                    new Thread(
                            () -> {
                                if (!IPS.contains(socket.getInetAddress()
                                        .getHostAddress())) {
                                    IPS.add(0, socket.getInetAddress()
                                            .getHostAddress());
                                }
                                String message = null;
                                BufferedWriter out = null;
                                try {
                                    out = new BufferedWriter(

                                            new OutputStreamWriter(
                                                    socket.getOutputStream()));
                                    byte[] buffer = new byte[1400];
                                    int r = socket.getInputStream().read(buffer);
                                    message = new String(buffer, "UTF-8")
                                            .substring(0, r);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    if (message.contains(
                                            OTHER.CHECK_SERVER)) {
                                        message = "yes";
                                        out.write(message);
                                        out.flush();
                                        System.out
                                                .println("Sent \tto TCP: \t"
                                                        + message);
                                        socket.close();
                                    } else {
                                        message = message.substring(0, message.lastIndexOf(
                                                "}") + 1);
                                        System.out.println(
                                                "received from TCP: \t " + message);
                                        TcpMessage tcpMessage = new Gson()
                                                .fromJson(message,
                                                        TcpMessage.class);
                                        switch (tcpMessage.getCommand()) {
                                            case TCP_COMMANDS.DEVICE.GET:
                                            case TCP_COMMANDS.DEVICE.SET: {
                                                Device device = new Device();
                                                device.expand(tcpMessage.getBody());
                                                tcpMessage.setBody(
                                                        new Gson()
                                                                .toJson(device,
                                                                        Device.class));
                                                tcpMessage.setBodyClass(
                                                        "Device");
                                                tcpCallback
                                                        .onTcpMessageReceived(
                                                                tcpMessage);
                                                message = "OK";
                                                assert out != null;
                                                out.write(message);
                                                out.flush();
                                                System.out
                                                        .println(
                                                                "Sent \tto TCP: \t"
                                                                        + message);
                                                socket.close();
                                                break;
                                            }
                                            case ROOM.GET_ROOMS: {
                                                replayRooms(out, socket);
                                                break;
                                            }
                                            case OTHER.RESET: {
                                                reset(out, socket);
                                                break;
                                            }
                                            case OTHER.REBOOT: {
                                                reboot(out, socket);
                                                break;
                                            }
                                            default: {
                                                tcpCallback
                                                        .onTcpMessageReceived(
                                                                tcpMessage);
                                                message = "OK";
                                                out.write(message);
                                                out.flush();
                                                System.out
                                                        .println(
                                                                "Sent \tto TCP: \t"
                                                                        + message);
                                                socket.close();
                                                break;
                                            }
                                        }
                                    }
                                } catch (IOException | ClassNotFoundException e) {
                                    e.printStackTrace();
                                }
                            }
                    ).start();
                }
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
        }).start();
    }

    private void reset(BufferedWriter out, Socket socket) {
        try {
            String message = "reset OK";
            out.write(message);
            out.flush();
            tcpCallback.onTcpMessageReceived(new TcpMessage() {{
                setCommand(OTHER.RESET);
                setBodyClass("Other");
            }});
            System.out
                    .println(
                            "Sent \tto TCP: \t"
                                    + message);
            socket.close();
        } catch (IOException | ClassNotFoundException ignored) {
        }
    }

    private void reboot(BufferedWriter out, Socket socket) {
        try {
            String message = "reboot OK";
            out.write(message);
            out.flush();
            System.out
                    .println(
                            "Sent \tto TCP: \t"
                                    + message);
            socket.close();
            Runtime.getRuntime().exec("reboot");
        } catch (IOException ignored) {
        }
    }

    private void replayRooms(BufferedWriter out, Socket socket) {
        try {
            for (Room room : db.roomDuo().ROOMS()) {
                String message = new Gson().toJson(room, Room.class);
                out.write(message);
                out.flush();
                System.out
                        .println(
                                "Sent \tto TCP: \t"
                                        + message);
                Thread.sleep(200);
            }
            socket.close();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }


    }

    public interface TcpCallback {

        void onTcpMessageReceived(TcpMessage tcpMessage) throws ClassNotFoundException;
    }

}
