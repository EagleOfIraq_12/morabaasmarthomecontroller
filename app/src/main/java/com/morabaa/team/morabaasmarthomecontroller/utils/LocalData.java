package com.morabaa.team.morabaasmarthomecontroller.utils;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class LocalData {
      
      @SuppressLint("StaticFieldLeak")
      private static LocalData instance;
      private Context ctx;
      
      public LocalData(Context ctx) {
            this.ctx = ctx;
            setInstance(this);
      }
      
      public static LocalData getInstance() {
            return instance;
      }
      
      private static void setInstance(LocalData instance) {
            LocalData.instance = instance;
      }
      
      public static LocalData getInstance(Context ctx) {
            return instance = instance == null ? new LocalData(ctx) : instance;
      }
      
      public String get(String name) {
            return getSharedPreferences(ctx).getString(name, "");
      }
      
      public int getInt(String name) {
            return getSharedPreferences(ctx).getInt(name, 0);
      }
      
      private SharedPreferences getSharedPreferences(Context ctx) {
            return PreferenceManager.getDefaultSharedPreferences(ctx);
      }
      
      public void add(String name, String content) {
            SharedPreferences editor = getSharedPreferences(ctx);
            editor
                  .edit()
                  .putString(name, content)
                  .apply();
      }
      
      public void add(String name, int content) {
            SharedPreferences editor = getSharedPreferences(ctx);
            editor
                  .edit()
                  .putInt(name, content)
                  .apply();
      }
      
      public void remove(String name) {
            SharedPreferences editor = getSharedPreferences(ctx);
            editor
                  .edit()
                  .remove(name)
                  .apply();
      }
}
