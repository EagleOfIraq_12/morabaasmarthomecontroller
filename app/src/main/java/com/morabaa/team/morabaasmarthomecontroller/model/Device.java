package com.morabaa.team.morabaasmarthomecontroller.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import com.morabaa.team.morabaasmarthomecontroller.utils.SV.TCP_COMMANDS.DEVICE;
import java.util.List;

/**
 * Created by eagle on 10/9/2017.
 */
@Entity
public class Device {
      
      @PrimaryKey
      private long id;
      private long deviceId;
      private String name;
      private long controllerId;
      private long currentDeviceId;
      private long roomId;
      private String type = "single";
      private int status = 0;
      private String iconName = "device_e";
      private int priority;
      private float amperes;
      
      public Device() {
      }
      
      public Device(long deviceId, long controllerId, int status) {
            this.deviceId = deviceId;
            this.controllerId = controllerId;
            this.status = status;
      }
      
      public static String minify(Device device) {
            
            return device.getControllerId()
                  + "," +
                  device.getDeviceId()
                  + "," +
                  device.getStatus();
      }
      
      public void expand(String deviceString) {
            String[] body = deviceString.split(",");
            long c = Long.parseLong(body[0]);
            long d = Long.parseLong(body[1]);
            int s = Integer.parseInt(body[2]);
            this.setDeviceId(d);
            this.setControllerId(c);
            this.setStatus(s);
      }
      
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public long getDeviceId() {
            return deviceId;
      }
      
      public void setDeviceId(long deviceId) {
            this.deviceId = deviceId;
      }
      
      public String getName() {
            return name;
      }
      
      public void setName(String name) {
            this.name = name;
      }
      
      public long getControllerId() {
            return controllerId;
      }
      
      public void setControllerId(long controllerId) {
            this.controllerId = controllerId;
      }
      
      public long getRoomId() {
            return roomId;
      }
      
      public void setRoomId(long roomId) {
            this.roomId = roomId;
      }
      
      public String getType() {
            return type;
      }
      
      public void setType(String type) {
            this.type = type;
      }
      
      public int getStatus() {
            return status;
      }
      
      public void setStatus(int status) {
            this.status = status;
      }
      
      public String getIconName() {
            return iconName;
      }
      
      public void setIconName(String iconName) {
            this.iconName = iconName;
      }
      
      public int getPriority() {
            return priority;
      }
      
      public void setPriority(int priority) {
            this.priority = priority;
      }
      
      public float getAmperes() {
            return amperes;
      }
      
      public void setAmperes(float amperes) {
            this.amperes = amperes;
      }
      
      public long getCurrentDeviceId() {
            return currentDeviceId;
      }
      
      public void setCurrentDeviceId(long currentDeviceId) {
            this.currentDeviceId = currentDeviceId;
      }
      
      public String minify() {
            
            return this.getControllerId()
                  + "," +
                  this.getDeviceId()
                  + "," +
                  this.getStatus();
      }
      
      public TcpMessage set() {
            TcpMessage tcpMessage = new TcpMessage();
            tcpMessage.setBody(this.minify());
            tcpMessage.setBodyClass("Device");
            tcpMessage.setCommand(DEVICE.SET);
            return tcpMessage;
            
      }
      
      public TcpMessage get() {
            TcpMessage tcpMessage = new TcpMessage();
            tcpMessage.setBody(this.minify());
            tcpMessage.setBodyClass("Device");
            tcpMessage.setCommand(DEVICE.GET);
            return tcpMessage;
      }
      
      @Dao
      public interface DeviceDuo {
            
            @Query("SELECT * FROM Device")
            List<Device> DEVICES();
            
            @Query("DELETE FROM Device")
            void deleteAll();
            
            @Query("SELECT * FROM Device WHERE id = :id")
            Device getDeviceById(long id);
            
            @Query("SELECT * FROM Device WHERE roomId = :roomId")
            List<Device> getDevicesByRoomId(long roomId);
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insert(Device... devices);
            
            
            @Delete
            void delete(Device... devices);
            
            @Update
            void Update(Device... devices);
            
            @Query("SELECT COUNT(id) FROM Device")
            int count();
      }
      
}
