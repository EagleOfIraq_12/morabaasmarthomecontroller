package com.morabaa.team.morabaasmarthomecontroller;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.morabaa.team.morabaasmarthomecontroller.model.Device;
import com.morabaa.team.morabaasmarthomecontroller.model.TcpMessage;
import com.morabaa.team.morabaasmarthomecontroller.utils.Contents;
import com.morabaa.team.morabaasmarthomecontroller.utils.QRCodeEncoder;
import com.morabaa.team.morabaasmarthomecontroller.utils.SV;

public class QRActivity extends Activity {

    ImageView imgQR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);

        imgQR = findViewById(R.id.imgQR);
        Device device = new Device();
        device.setName(SV.GET_DEVICE_ID(QRActivity.this));
        device.setType(SV.GET_DEVICE_IP(QRActivity.this));
        TcpMessage tcpMessage = new TcpMessage();
        tcpMessage.setCommand("key");
        tcpMessage.setBody(new Gson().toJson(device,Device.class));
        String s = new Gson().toJson(tcpMessage, TcpMessage.class);
        generateQR(s);
    }

    void generateQR(String qrText) {
        System.out.println("QRText: " + qrText);
        int qrCodeDimension = 1500;

        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(
                qrText,
                null,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                qrCodeDimension
        );
        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            imgQR.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public void back(View view) {
        Intent intent = new Intent(QRActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
