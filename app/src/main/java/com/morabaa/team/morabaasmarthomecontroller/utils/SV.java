package com.morabaa.team.morabaasmarthomecontroller.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.util.DisplayMetrics;


/**
 * Created by eagle on 1/16/2018.
 */

public class SV {
      
      
      public static String DEVICE_ID = "";
      static String ip = "";
      
      @SuppressLint("HardwareIds")
      public static String GET_DEVICE_ID(Context context) {
            return Secure.getString(context.getContentResolver(),
                  Secure.ANDROID_ID);
      }
      
      public static void setIP(Context context) {
            if (Settings.System.canWrite(context)) {
                  // change setting here
                  final ContentResolver cr = context.getContentResolver();
                  Settings.System.putInt(cr, Settings.System.WIFI_USE_STATIC_IP, 1);
                  Settings.System.putString(cr, Settings.System.WIFI_STATIC_IP, "192.168.0.133");
                  java.lang.System.out
                        .println("setIP: " + Settings.System.getString(cr, System.WIFI_STATIC_IP));
            } else {
                  //Migrate to Setting write permission screen.
                  Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                  intent.setData(Uri.parse("package:" + context.getPackageName()));
                  context.startActivity(intent);
                  final ContentResolver cr = context.getContentResolver();
                  Settings.System.putInt(cr, Settings.System.WIFI_USE_STATIC_IP, 1);
                  Settings.System.putString(cr, Settings.System.WIFI_STATIC_IP, "192.168.0.133");
                  java.lang.System.out
                        .println("setIP: " + Settings.System.getString(cr, System.WIFI_STATIC_IP));
            }
            
      }
      
      public static String GET_DEVICE_IP(Context ctx) {
            new Thread(
                  new Runnable() {
                        @Override
                        public void run() {
                              ip = ConnectionState.getLocalIpAddress();
                        }
                  }
            ).start();
            return ip;
      }
      
      public static class TCP_COMMANDS {
            
            
            public static class DEVICE {
                  
                  public static final String GET = "get";
                  public static final String SET = "set";
                  public static final String UPDATE = "updateDevice";
                  public static final String ADD = "addDevice";
                  public static final String GET_ALL = "getDevices";
                  public static final String TURN_ALL_ON = "turnAllOn";
                  public static final String TURN_ALL_OFF = "turnAllOff";
            }
            
            public static class ROOM {
                  
                  public static final String GET_ROOMS = "getRooms";
                  public static final String TURN_ROOM_OFF = "turnRoomOff";
                  public static final String TURN_ROOM_ON = "turnRoomOn";
                  public static final String GET_ROOM_DEVICES = "getRoomDevices";
            }
            
            public static class RELATION {
                  
                  public static final String ADD = "addRelation";
            }
            
            public static class OTHER {
                  
                  public static final String REFRESH = "refresh";
                  public static final String CHECK_SERVER = "areYouServer";
                  public static final String DEBUG = "debug";
                  public static final String RESET = "reset";
                  public static final String REBOOT = "reboot";
      
            }
            
      }
      
      public static class ScreenDimensions {
            
            public static int getHeight(Activity activity) {
                  DisplayMetrics displayMetrics = new DisplayMetrics();
                  activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                  return displayMetrics.heightPixels;
            }
            
            public static int getWidth(Activity activity) {
                  DisplayMetrics displayMetrics = new DisplayMetrics();
                  activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                  return displayMetrics.widthPixels;
            }
      }
      
}
