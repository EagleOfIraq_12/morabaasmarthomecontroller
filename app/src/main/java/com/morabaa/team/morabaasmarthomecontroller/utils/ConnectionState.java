package com.morabaa.team.morabaasmarthomecontroller.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * Created by eagle on 12/7/2017.
 */

public class ConnectionState {
    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                 en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses();
                     enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        InetAddress address = inetAddress;
                        return intf.getInterfaceAddresses().get(1).getAddress()
                                .getHostName();
//                                    return address.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("ip", ex.toString());
            return "192.168.10.222";
        }
        return "192.168.10.222";

    }

    public static String getBssid(Context ctx) {
        WifiManager wifiManager = (WifiManager)
                ctx.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifiManager.getConnectionInfo();
        return "C8:3A:35:1B:E9:40".toUpperCase();

//        if (wInfo.getBSSID() != null) {
//            return wInfo.getBSSID().toUpperCase();
//        }
//        return "00:00:00:00:00:00";
    }

    public static boolean isWifi(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isConnected()
                && info.getType() == ConnectivityManager.TYPE_WIFI;
    }
}
