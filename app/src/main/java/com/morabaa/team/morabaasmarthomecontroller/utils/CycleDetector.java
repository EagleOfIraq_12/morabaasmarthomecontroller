package com.morabaa.team.morabaasmarthomecontroller.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CycleDetector {


    private boolean marked[];
    private boolean onStack[];
    private List<Long>[] adj_data;

    public CycleDetector(List[] adj, int n) {
        adj_data = adj;
        marked = new boolean[n];
        onStack = new boolean[n];
    }

    public String findCycleForVertex() {
        int v=1;
        String result = detectCycles(v, v);
        if (result == "") {
            // the case of no cycles detected:
            return "no cycles found for vertex " + v;
        } else {
            String[] temp = result.split("\\s+");
            Collections.reverse(Arrays.asList(temp));
            return "cycle found!\n" + Arrays.toString(temp);
        }
    }
    private String detectCycles(int v, long original) {
        marked[v] = true;
        onStack[v] = true;
        if (adj_data[v] == null) {
            return "";
        }
        for (Long li : adj_data[v]) {
            int i= Math.toIntExact(li);
            if (marked[ i] == false) {
                String res = detectCycles(i, original);
                if (res != "") {
                    return res + " " + v;
                }
            } else if (onStack[i] == true && i == original) {
                return "" + v;
            }
        }
        onStack[v] = false;
        return "";
    }
}