package com.morabaa.team.morabaasmarthomecontroller.model;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DatabaseReference.CompletionListener;
import com.morabaa.team.morabaasmarthomecontroller.MainActivity;
import com.morabaa.team.morabaasmarthomecontroller.utils.ConnectionState;
import java.util.Date;

/**
 * Created by eagle on 3/12/2018.
 */

public class FirebaseMessage {
      
      private String message;
      private boolean wifi;
      private String bssid;
      private Date date = new Date();
      
      public FirebaseMessage() {
      }
      
      public static void send(final String message) {
            new Thread(new Runnable() {
                  @Override
                  public void run() {
                        FirebaseMessage firebaseMessage = new FirebaseMessage();
                        firebaseMessage.setMessage(message);
                        firebaseMessage
                              .setBssid(ConnectionState.getBssid(MainActivity.ctx));
                        firebaseMessage
                              .setWifi(ConnectionState.isWifi(MainActivity.ctx));
                        MainActivity.fromControllerMessageRef
                              .setValue(firebaseMessage, new CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError,
                                          DatabaseReference databaseReference) {
                                          if (databaseError != null) {
                                                System.out.println(
                                                      "Firebase Error:\t"
                                                            + databaseError
                                                            .getMessage());
                                          }
                                          if (databaseReference != null) {
                                                System.out.println(
                                                      "Sent to Firebase:\t" + message);
                                          }
                                    }
                              });
                  }
            }).start();
      }
      
      public String getMessage() {
            return message;
      }
      
      public void setMessage(String message) {
            this.message = message;
      }
      
      public boolean isWifi() {
            return wifi;
      }
      
      public void setWifi(boolean wifi) {
            this.wifi = wifi;
      }
      
      public String getBssid() {
            return bssid;
      }
      
      public void setBssid(String bssid) {
            this.bssid = bssid;
      }
      
      public Date getDate() {
            return date;
      }
      
      public void setDate(Date date) {
            this.date = date;
      }
}
