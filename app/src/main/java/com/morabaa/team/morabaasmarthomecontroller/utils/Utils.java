package com.morabaa.team.morabaasmarthomecontroller.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * Created by eagle on 3/5/2018.
 */

public class Utils {
      
      public static Drawable getDrawableByName(Context context, String name) {
            int resourceId = context.getResources()
                  .getIdentifier(name, "drawable", context.getPackageName());
            return context.getResources().getDrawable(resourceId);
      }
      
      public static String getDrawableNameById(Context context, int resId) {
            return context.getResources().getResourceEntryName(resId);
      }
}
