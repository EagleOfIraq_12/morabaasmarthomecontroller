package com.morabaa.team.morabaasmarthomecontroller.services;

      import android.os.Build;
      import android.os.Handler;
      import android.os.HandlerThread;
      import com.google.android.things.pio.Gpio;
      import com.google.android.things.pio.PeripheralManager;
      import com.google.android.things.pio.UartDevice;
      import com.google.android.things.pio.UartDeviceCallback;
      import com.google.gson.Gson;
      import com.morabaa.team.morabaasmarthomecontroller.MainActivity;
      import com.morabaa.team.morabaasmarthomecontroller.model.UARTMessage;
      import java.io.IOException;
      import java.util.Arrays;
      import java.util.Date;
      import java.util.concurrent.ExecutorService;
      import java.util.concurrent.Executors;

/**
 * Created by eagle on 2/14/2018.
 */

public class UARTServer {

      private static final byte STOP_BYTE = -87;
      private static final byte START_BYTE = -106;
      private static final byte CONTROLLER_TYPE_BYTE = -125; //master: -125 ,slave :
      private static final byte CRC_BYTE = 1;
      private static final int BAUD_RATE = 9600;
      private static final int DATA_BITS = 8;
      private static final int STOP_BITS = 1;
      private static final String DEVICE_RPI3 = "rpi3";
      private static final String DEVICE_IMX6UL_PICO = "imx6ul_pico";
      private static final String DEVICE_IMX7D_PICO = "imx7d_pico";

      public static UARTServer instant;
      public static Gpio mGpio;
      public static Gpio fGpio;
      private static UartDevice mLoopbackDevice;
      private static ExecutorService executor = Executors.newFixedThreadPool(1);
      private UartCallback uartCallback;
      private UartDeviceCallback mCallback = new UartDeviceCallback() {
            @Override
            public boolean onUartDeviceDataAvailable(UartDevice uart) {
                  // Queue up a data transfer
                  transferUartData();
                  //Continue listening for more interrupts
                  return true;
            }

            @Override
            public void onUartDeviceError(UartDevice uart, int error) {
                  System.out.println(uart + ": Error event " + error);
            }
      };
      /* Private Helper Methods */
      private Runnable mTransferUartRunnable = this::transferUartData;

      public UARTServer() {

            System.out.println("UARTServer Started at :\t" + new Date());
//            setUartCallback(MainActivity.instance);
//            setInstant(this);
            PeripheralManager manager = PeripheralManager.getInstance();
            try {
                  mGpio = manager.openGpio("BCM18");
                  fGpio = manager.openGpio("BCM21");
                  mGpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
                  fGpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
                  fGpio.setValue(true);

                  System.out.println("BCM18 : " + mGpio.getName() + "\t" + mGpio.getValue());
                  System.out.println("BCM21 : " + fGpio.getName() + "\t" + fGpio.getValue());

            } catch (IOException e) {
                  System.out.println("Unable to access GPIO " + e);
            }
            HandlerThread mInputThread = new HandlerThread("InputThread");
            mInputThread.start();
            Handler mInputHandler = new Handler(mInputThread.getLooper());
            try {
                  mLoopbackDevice = manager.openUartDevice(getUartName());
                  System.out.println(" opening Loopback " + mLoopbackDevice.getName());
                  // Configure the UART
                  mLoopbackDevice.setBaudrate(BAUD_RATE);
                  mLoopbackDevice.setDataSize(DATA_BITS);
                  mLoopbackDevice.setParity(UartDevice.PARITY_NONE);
                  mLoopbackDevice.setStopBits(STOP_BITS);
                  mLoopbackDevice.registerUartDeviceCallback(mInputHandler, mCallback);
                  mInputHandler.post(mTransferUartRunnable);
                  transferUartData();
                  System.out.println("Loopback Created");
            } catch (IOException e) {
                  System.out.println("Loopback Error "+e.getMessage() );

            }

      }

      public static UARTServer getInstant() {
            return instant;
      }

      public static void setInstant(
            UARTServer instant) {
            UARTServer.instant = instant;
      }

      public static String getUartName() {
            switch (Build.DEVICE) {
                  case DEVICE_RPI3:
                        return "UART0";
                  case DEVICE_IMX6UL_PICO:
                        return "UART3";
                  case DEVICE_IMX7D_PICO:
                        return "UART6";
                  default:
                        throw new IllegalStateException("Unknown Build.DEVICE " + Build.DEVICE);
            }
      }

      private static byte[] updateCRC(byte[] messageBytes) {
            byte crcByte = (byte) (messageBytes[2] ^ messageBytes[1]);
            crcByte ^= messageBytes[3];
            crcByte ^= messageBytes[4];
            crcByte ^= messageBytes[5];
            crcByte = (byte) ~crcByte;
            if ((crcByte == messageBytes[0]) || (crcByte == messageBytes[7])) {
                  crcByte++;
            }
            messageBytes[6] = crcByte;
            return messageBytes;
      }

      public static void send(final UARTMessage uartMessage) {
            MainActivity.instance.runOnUiThread(() ->
                  MainActivity.instance.txtUartMessagesOut
                        .setText(uartMessage.toString() + "\n" +
                              MainActivity.instance.txtUartMessagesOut.getText()
                        ));

            Runnable runnable = () -> {
                  try {
                        execute(uartMessage);
                        Thread.sleep(150);
                  } catch (InterruptedException e) {
                        e.printStackTrace();
                  }
            };
            executor.submit(runnable);
      }

      private static void execute(UARTMessage uartMessage) {
            try {
                  if (mLoopbackDevice != null) {
                        byte[] buffer = {
                              START_BYTE,
                              (byte) uartMessage.getControllerAddress(),
                              CONTROLLER_TYPE_BYTE,
                              (byte) uartMessage.getCmd().charAt(0),
                              (byte) uartMessage.getDeviceAddress(),
                              (byte) uartMessage.getState(),
                              CRC_BYTE,
                              STOP_BYTE
                        };
                        buffer = updateCRC(buffer);
                        mGpio.setActiveType(Gpio.ACTIVE_HIGH);
                        fGpio.setActiveType(Gpio.ACTIVE_HIGH);
                        mGpio.setValue(true);
                        mLoopbackDevice.write(buffer, buffer.length);
                        String message = new Gson()
                              .toJson(uartMessage, UARTMessage.class);
                        System.out.println(
                              "Sent to c: \t" + message + "\t " + Arrays.toString(buffer)
                        );
                        Thread.sleep(10);
                        mGpio.setValue(false);

                  }
            } catch (IOException | InterruptedException e) {
                  System.out.println("Sent to c: \t" + e.getMessage());
            }
      }


      public void setUartCallback(
            UartCallback uartCallback) {
//            this.uartCallback = MainActivity.instance;
            this.uartCallback = uartCallback;
      }


      private void transferUartData() {
            if (mLoopbackDevice != null) {
                  try {
                        final byte[] buffer = new byte[8];
                        while ((mLoopbackDevice.read(buffer, buffer.length)) > 0) {

                              //  buffer[0]; => Start
                              //  buffer[1]; => ControllerAddress
                              //  buffer[2]; => ControllerType
                              //  buffer[3]; => GetOrSet
                              //  buffer[4]; => DeviceAddress
                              //  buffer[5]; => State
                              //  buffer[6]; => CRC
                              //  buffer[7]; => Stop

                              if (buffer[6] != 0 && buffer[6] == updateCRC(buffer)[6]) {
                                    System.out.println(
                                          "Receive from c: " + Arrays
                                                .toString(buffer)
                                    );
                                    UARTMessage uartMessage = new UARTMessage() {{
                                          setControllerAddress(buffer[1]);
                                          setCmd(new String(new byte[]{buffer[3]}, "UTF-8"));
                                          setDeviceAddress(buffer[4]);
                                          setState(buffer[5]);
                                    }};
                                    uartCallback.onUartMessageReceived(uartMessage);
                                    if (buffer[1] == 112) {
                                          uartCallback.onUartBufferReceived((buffer[3]),
                                                buffer[4],
                                                buffer[5]);
                                    }
                              } else {
//                                    System.out.println(
//                                          "Receive from c with Error : " + Arrays
//                                                .toString(buffer)
//                                    );
                              }
                        }
                  } catch (IOException e) {
                        System.out.println("Unable to transfer data over UART");
                  }
            }
      }

      public interface UartCallback {

            void onUartMessageReceived(UARTMessage uartMessage);

            void onUartBufferReceived(byte... uartBuffer);

      }

}
