package com.morabaa.team.morabaasmarthomecontroller.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

/**
 * Created by eagle on 2/14/2018.
 */

public class UARTIntentService extends IntentService {
      
      public UARTIntentService(String name) {
            super(name);
      }
      
      @Override
      protected void onHandleIntent(@Nullable Intent intent) {
      
      }
}/*
      
      public static final byte STOP_BYTE = -87;
      public static final byte START_BYTE = -106;
      public static final byte CONTROLLER_TYPE_BYTE = -125; //master: -125 ,slave :
      public static final byte CRC_BYTE = 1;
      private static final int BAUD_RATE = 9600;
      private static final int DATA_BITS = 8;
      private static final int STOP_BITS = 1;
      private static final String DEVICE_RPI3 = "rpi3";
      private static final String DEVICE_IMX6UL_PICO = "imx6ul_pico";
      private static final String DEVICE_IMX7D_PICO = "imx7d_pico";
      
      public static UARTIntentService instant;
      public static Gpio mGpio;
      public static Gpio fGpio;
      private static UartDevice mLoopbackDevice;
      private static ExecutorService executor = Executors.newFixedThreadPool(1);
      private UartCallback uartCallback;
      private UartDeviceCallback mCallback = new UartDeviceCallback() {
            @Override
            public boolean onUartDeviceDataAvailable(UartDevice uart) {
                  // Queue up a data transfer
                  transferUartData();
                  //Continue listening for more interrupts
                  return true;
            }
            
            @Override
            public void onUartDeviceError(UartDevice uart, int error) {
                  System.out.println(uart + ": Error event " + error);
            }
      };
      *//* Private Helper Methods *//*
      private Runnable mTransferUartRunnable = this::transferUartData;
      
      public UARTIntentService() {
            super("");
      }
      
      public static UARTIntentService getInstant() {
            return instant;
      }
      
      public static void setInstant(
            UARTIntentService instant) {
            UARTIntentService.instant = instant;
      }
      
      public static String getUartName() {
            switch (Build.DEVICE) {
                  case DEVICE_RPI3:
                        return "UART0";
                  case DEVICE_IMX6UL_PICO:
                        return "UART3";
                  case DEVICE_IMX7D_PICO:
                        return "UART6";
                  default:
                        throw new IllegalStateException("Unknown Build.DEVICE " + Build.DEVICE);
            }
      }
      
      private static byte[] updateCRC(byte[] messageBytes) {
            byte crcByte = (byte) (messageBytes[2] ^ messageBytes[1]);
            crcByte ^= messageBytes[3];
            crcByte ^= messageBytes[4];
            crcByte ^= messageBytes[5];
            crcByte = (byte) ~crcByte;
            if ((crcByte == messageBytes[0]) || (crcByte == messageBytes[7])) {
                  crcByte++;
            }
            messageBytes[6] = crcByte;
            return messageBytes;
      }
      
      public static void send(final UARTMessage uartMessage) {
            MainActivity.instance.runOnUiThread(() ->
                  MainActivity.instance.txtUartMessagesOut
                        .setText(uartMessage.toString() + "\n" +
                              MainActivity.instance.txtUartMessagesOut.getText()
                        ));
            
            Runnable runnable = () -> {
                  try {
                        execute(uartMessage);
                        Thread.sleep(150);
                  } catch (InterruptedException e) {
                        e.printStackTrace();
                  }
            };
            executor.submit(runnable);
      }
      
      private static void execute(UARTMessage uartMessage) {
            try {
                  if (mLoopbackDevice != null) {
                        byte[] buffer = {
                              START_BYTE,
                              (byte) uartMessage.getControllerAddress(),
                              CONTROLLER_TYPE_BYTE,
                              (byte) uartMessage.getCmd().charAt(0),
                              (byte) uartMessage.getDeviceAddress(),
                              (byte) uartMessage.getState(),
                              CRC_BYTE,
                              STOP_BYTE
                        };
                        buffer = updateCRC(buffer);
                        mGpio.setActiveType(Gpio.ACTIVE_HIGH);
                        fGpio.setActiveType(Gpio.ACTIVE_HIGH);
                        mGpio.setValue(true);
                        mLoopbackDevice.write(buffer, buffer.length);
                        String message = new Gson()
                              .toJson(uartMessage, UARTMessage.class);
                        System.out.println(
                              "Sent to c: \t" + message + "\t " + Arrays.toString(buffer)
                        );
                        Thread.sleep(10);
                        mGpio.setValue(false);
                        
                  }
            } catch (IOException | InterruptedException e) {
                  System.out.println("Sent to c: \t" + e.getMessage());
            }
      }
      
      @Override
      public void onCreate() {
            // TODO: It would be nice to have an option to hold a partial wakelock
            // during processing, and to have a static startService(Context, Intent)
            // method that would launch the service & hand off a wakelock.
            
            super.onCreate();
            System.out.println("UARTService Started at :\t" + new Date());
            setUartCallback(MainActivity.instance);
//            setInstant(this);
            PeripheralManager manager = PeripheralManager.getInstance();
            try {
                  mGpio = manager.openGpio("BCM18");
                  fGpio = manager.openGpio("BCM21");
                  mGpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
                  fGpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
                  fGpio.setValue(true);
                  
                  System.out.println("BCM18 : " + mGpio.getName() + "\t" + mGpio.getValue());
                  System.out.println("BCM21 : " + fGpio.getName() + "\t" + fGpio.getValue());
                  
            } catch (IOException e) {
                  System.out.println("Unable to access GPIO " + e);
            }
            HandlerThread mInputThread = new HandlerThread("InputThread");
            mInputThread.start();
            Handler mInputHandler = new Handler(mInputThread.getLooper());
            try {
                  mLoopbackDevice = manager.openUartDevice(getUartName());
                  System.out.println(" opening Loopback " + mLoopbackDevice.getName());
                  // Configure the UART
                  mLoopbackDevice.setBaudrate(BAUD_RATE);
                  mLoopbackDevice.setDataSize(DATA_BITS);
                  mLoopbackDevice.setParity(UartDevice.PARITY_NONE);
                  mLoopbackDevice.setStopBits(STOP_BITS);
                  mLoopbackDevice.registerUartDeviceCallback(mInputHandler, mCallback);
                  mInputHandler.post(mTransferUartRunnable);
                  transferUartData();
                  System.out.println("Loopback Created");
            } catch (IOException e) {
                  System.out.println("Loopback Error");
                  
            }
            
      }
      
      public void setUartCallback(
            UartCallback uartCallback) {
//            this.uartCallback = MainActivity.instance;
            this.uartCallback = uartCallback;
      }
      
      @Nullable
      @Override
      public IBinder onBind(Intent intent) {
            return null;
      }
      
      @Override
      protected void onHandleIntent(@Nullable Intent intent) {
      
      }
      
      @Override
      public int onStartCommand(Intent intent, int flags, int startId) {
            
            return Service.START_STICKY;
      }
      
      private void transferUartData() {
            if (mLoopbackDevice != null) {
                  try {
                        final byte[] buffer = new byte[8];
                        while ((mLoopbackDevice.read(buffer, buffer.length)) > 0) {
                              
                              //  buffer[0]; => Start
                              //  buffer[1]; => ControllerAddress
                              //  buffer[2]; => ControllerType
                              //  buffer[3]; => GetOrSet
                              //  buffer[4]; => DeviceAddress
                              //  buffer[5]; => State
                              //  buffer[6]; => CRC
                              //  buffer[7]; => Stop
                              
                              if (buffer[6] != 0 && buffer[6] == updateCRC(buffer)[6]) {
                                    System.out.println(
                                          "Receive from c: " + Arrays
                                                .toString(buffer)
                                    );
                                    UARTMessage uartMessage = new UARTMessage() {{
                                          setControllerAddress(buffer[1]);
                                          setCmd(new String(new byte[]{buffer[3]}, "UTF-8"));
                                          setDeviceAddress(buffer[4]);
                                          setState(buffer[5]);
                                    }};
                                    uartCallback.onUartMessageReceived(uartMessage);
                                    if (buffer[1] == 112) {
                                          uartCallback.onUartBufferReceived((buffer[3]),
                                                buffer[4],
                                                buffer[5]);
                                    }
                              } else {
                                    System.out.println(
                                          "Receive from c with Error : " + Arrays
                                                .toString(buffer)
                                    );
                              }
                        }
                  } catch (IOException e) {
                        System.out.println("Unable to transfer data over UART");
                  }
            }
      }
      
      *//**
       * Called by the system to notify a Service that it is no longer used and is being removed.  The
       * service should clean up any resources it holds (threads, registered
       * receivers, etc) at this point.  Upon return, there will be no more calls
       * in to this Service object and it is effectively dead.  Do not call this method directly.
       * 06-27 15:42:21.889 9752-9752/? I/zygote: Late-enabling -Xcheck:jni
       * 06-27 15:42:22.252 9752-9752/com.morabaa.team.morabaasmarthomecontroller D/AndroidRuntime: Shutting down VM
       * 06-27 15:42:22.260 9752-9752/com.morabaa.team.morabaasmarthomecontroller E/AndroidRuntime: FATAL EXCEPTION: main
       * Process: com.morabaa.team.morabaasmarthomecontroller, PID: 9752
       * java.lang.RuntimeException: Unable to get provider com.google.firebase.provider.FirebaseInitProvider: java.lang.ClassNotFoundException: Didn't find class "com.google.firebase.provider.FirebaseInitProvider" on path: DexPathList[[zip file "/system/framework/com.google.android.things.jar", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/base.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_resources_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_0_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_1_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_2_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_3_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_4_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_5_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_6_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_7_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_8_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_9_apk.apk"],nativeLibraryDirectories=[/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/lib/arm, /system/lib, /vendor/lib]]
       * at android.app.ActivityThread.installProvider(ActivityThread.java:6242)
       * at android.app.ActivityThread.installContentProviders(ActivityThread.java:5805)
       * at android.app.ActivityThread.handleBindApplication(ActivityThread.java:5722)
       * at android.app.ActivityThread.-wrap1(Unknown Source:0)
       * at android.app.ActivityThread$H.handleMessage(ActivityThread.java:1656)
       * at android.os.Handler.dispatchMessage(Handler.java:106)
       * at android.os.Looper.loop(Looper.java:164)
       * at android.app.ActivityThread.main(ActivityThread.java:6494)
       * at java.lang.reflect.Method.invoke(Native Method)
       * at com.android.internal.os.RuntimeInit$MethodAndArgsCaller.run(RuntimeInit.java:438)
       * at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:807)
       * 06-27 15:42:22.268 9752-9752/com.morabaa.team.morabaasmarthomecontroller E/AndroidRuntime: Caused by: java.lang.ClassNotFoundException: Didn't find class "com.google.firebase.provider.FirebaseInitProvider" on path: DexPathList[[zip file "/system/framework/com.google.android.things.jar", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/base.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_resources_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_0_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_1_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_2_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_3_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_4_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_5_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_6_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_7_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_8_apk.apk", zip file "/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_slice_9_apk.apk"],nativeLibraryDirectories=[/data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/lib/arm, /system/lib, /vendor/lib]]
       * at dalvik.system.BaseDexClassLoader.findClass(BaseDexClassLoader.java:125)
       * at java.lang.ClassLoader.loadClass(ClassLoader.java:379)
       * at java.lang.ClassLoader.loadClass(ClassLoader.java:312)
       * at android.app.ActivityThread.installProvider(ActivityThread.java:6227)
       * ... 10 more
       * Suppressed: java.io.IOException: No original dex files found for dex location /data/app/com.morabaa.team.morabaasmarthomecontroller-wc4rWCotCiKH_82gSICEiA==/split_lib_resources_apk.apk
       * at dalvik.system.DexFile.openDexFileNative(Native Method)
       * at dalvik.system.DexFile.openDexFile(DexFile.java:353)
       * at dalvik.system.DexFile.<init>(DexFile.java:100)
       * at dalvik.system.DexFile.<init>(DexFile.java:74)
       * at dalvik.system.DexPathList.loadDexFile(DexPathList.java:374)
       * at dalvik.system.DexPathList.makeDexElements(DexPathList.java:337)
       * at dalvik.system.DexPathList.<init>(DexPathList.java:157)
       * at dalvik.system.BaseDexClassLoader.<init>(BaseDexClassLoader.java:65)
       * at dalvik.system.PathClassLoader.<init>(PathClassLoader.java:64)
       * at com.android.internal.os.ClassLoaderFactory.createClassLoader(ClassLoaderFactory.java:73)
       * at com.android.internal.os.ClassLoaderFactory.createClassLoader(ClassLoaderFactory.java:88)
       * at android.app.ApplicationLoaders.getClassLoader(ApplicationLoaders.java:69)
       * at android.app.ApplicationLoaders.getClassLoader(ApplicationLoaders.java:35)
       * at android.app.LoadedApk.createOrUpdateClassLoaderLocked(LoadedApk.java:693)
       * at android.app.LoadedApk.getClassLoader(LoadedApk.java:727)
       * at android.app.LoadedApk.getResources(LoadedApk.java:954)
       * at android.app.ContextImpl.createAppContext(ContextImpl.java:2270)
       * at android.app.ActivityThread.handleBindApplication(ActivityThread.java:5639)
       * ... 8 more
       * 06-27 15:42:22.295 9752-9752/com.morabaa.team.morabaasmarthomecontroller I/Process: Sending signal. PID: 9752 SIG: 9
       *//*
      public void onDestroy() {
            try {
                  fGpio.setValue(false);
            } catch (IOException ignored) {
            }
      }
      
      public interface UartCallback {
            
            void onUartMessageReceived(UARTMessage uartMessage);
            
            void onUartBufferReceived(byte... uartBuffer);
            
      }
      
}
*/