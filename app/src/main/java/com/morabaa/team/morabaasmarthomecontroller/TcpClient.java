package com.morabaa.team.morabaasmarthomecontroller;


import static com.morabaa.team.morabaasmarthomecontroller.MainActivity.IPS;

import com.google.gson.Gson;
import com.morabaa.team.morabaasmarthomecontroller.model.TcpMessage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by eagle on 2/11/2018.
 */

public class TcpClient {

    public TcpClient() {
    }

    public static void send(TcpMessage tcpMessage) {
        MainActivity.instance.runOnUiThread(() ->
                MainActivity.instance.txtTcpMessagesOut
                        .setText(tcpMessage.toString() + "\n" +
                                MainActivity.instance.txtTcpMessagesOut.getText()
                        ));

        String message = new Gson().toJson(tcpMessage, TcpMessage.class);
        message = message.substring(0, message.lastIndexOf("}") + 1);
        final String finalMessage = message;
        for (final String IP : IPS) {
            new Thread(() -> {
                Socket clientSocket = null;
                try {
                    if (InetAddress.getByName(IP).isReachable(300)) {
                        clientSocket = new Socket(IP, 21112);
                        if (!IP.equals(clientSocket.getLocalAddress().getHostAddress())) {
                            DataOutputStream outToServer = new DataOutputStream(
                                    clientSocket.getOutputStream());
                            System.out.println(
                                    "To " + clientSocket.getInetAddress() + " SERVER: "
                                            + finalMessage);
                            outToServer.write(finalMessage.getBytes());

                            byte[] buffer = new byte[1400];

                            int r = clientSocket.getInputStream().read(buffer);
                            if (r > 0) {
                                String newMessage = new String(buffer)
                                        .substring(0, r);
                                System.out.println("FROM SERVER: " + newMessage);
                            }
                        }
                        clientSocket.close();
                    } else {
                        System.out.println("IP = " + IP + " is not reachable");
                        send(tcpMessage);
                    }
                } catch (IOException e) {
                    System.out.println("Error : " + e.getMessage());
                }

            }).start();
        }
    }
}
