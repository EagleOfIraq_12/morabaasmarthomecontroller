package com.morabaa.team.morabaasmarthomecontroller;

import static android.content.ContentValues.TAG;
import static com.morabaa.team.morabaasmarthomecontroller.services.UARTServer.fGpio;
import static com.morabaa.team.morabaasmarthomecontroller.services.UARTServer.mGpio;
import static com.morabaa.team.morabaasmarthomecontroller.utils.Configs.getCurrentWiFiConfiguration;
import static com.morabaa.team.morabaasmarthomecontroller.utils.Configs.setStaticIpConfiguration;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DatabaseReference.CompletionListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.leinardi.android.things.driver.ds3231.Ds3231;
import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator;
import com.luckycatlabs.sunrisesunset.dto.Location;
import com.morabaa.team.morabaasmarthomecontroller.model.Device;
import com.morabaa.team.morabaasmarthomecontroller.model.FirebaseMessage;
import com.morabaa.team.morabaasmarthomecontroller.model.Floor;
import com.morabaa.team.morabaasmarthomecontroller.model.Relation;
import com.morabaa.team.morabaasmarthomecontroller.model.Room;
import com.morabaa.team.morabaasmarthomecontroller.model.TcpMessage;
import com.morabaa.team.morabaasmarthomecontroller.model.UARTMessage;
import com.morabaa.team.morabaasmarthomecontroller.services.TcpServerService;
import com.morabaa.team.morabaasmarthomecontroller.services.TcpServerService.TcpCallback;
import com.morabaa.team.morabaasmarthomecontroller.services.UARTServer;
import com.morabaa.team.morabaasmarthomecontroller.services.UARTServer.UartCallback;
import com.morabaa.team.morabaasmarthomecontroller.utils.ConnectionState;
import com.morabaa.team.morabaasmarthomecontroller.utils.CycleDetector;
import com.morabaa.team.morabaasmarthomecontroller.utils.DB;
import com.morabaa.team.morabaasmarthomecontroller.utils.LocalData;
import com.morabaa.team.morabaasmarthomecontroller.utils.SV;
import com.morabaa.team.morabaasmarthomecontroller.utils.SV.TCP_COMMANDS;
import com.morabaa.team.morabaasmarthomecontroller.utils.SV.TCP_COMMANDS.DEVICE;
import com.morabaa.team.morabaasmarthomecontroller.utils.SV.TCP_COMMANDS.OTHER;
import com.morabaa.team.morabaasmarthomecontroller.utils.SV.TCP_COMMANDS.ROOM;
import com.morabaa.team.morabaasmarthomecontroller.utils.Utils;

import java.io.IOException;
import java.net.InetAddress;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
/*
 * 123
 * */
// LAN MAC B8:27:EB:99:93:50


/* door 31
 * fan 20
 * temp 30
 * humidity 33
 * motion 32
 * buzzer 34 on off
 * buzzer 35 second
 * buzzer 36 start
 * buzzer 37 end
 */

/**
 * Skeleton of an Android Things activity.
 * <p>
 * Android Things peripheral APIs are accessible through the class
 * PeripheralManagerService. For example, the snippet below will open a GPIO pin and
 * set it to HIGH:
 * <p>
 * <pre>{@code
 * PeripheralManagerService service = new PeripheralManagerService();
 * mLedGpio = service.openGpio("BCM6");
 * mLedGpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
 * mLedGpio.setValue(true);
 * }</pre>
 * <p>
 * For more complex peripherals, look for an existing user-space driver, or implement one if none
 * is available.
 *
 * @see <a href="https://github.com/androidthings/contrib-drivers#readme">https://github.com/androidthings/contrib-drivers#readme</a>
 */
public class MainActivity extends Activity implements TcpCallback, UartCallback {

    public static List<String> IPS = new ArrayList<>();
    @SuppressLint("StaticFieldLeak")
    public static SeekBar seekRefresh;
    public static DatabaseReference fromControllerMessageRef;
    public static Context ctx;
    public static MainActivity instance;
    public static DB db;
    public static TextView txtTcpMessages;
    public static ScrollView scrollTcpMessages;
    public static TextView txtUartMessages;
    public static ScrollView scrollUartMessages;
    private NSDListen mNSDListener;

    public static TextView txtTcpMessagesOut;
    public static ScrollView scrollUartMessagesOut;
    public ScrollView scrollTcpMessagesOut;
    public TextView txtUartMessagesOut;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference firebaseReference;
    DatabaseReference fromPhoneMessageRef;
    DatabaseReference devicesRef;
    DatabaseReference roomsRef;
    DatabaseReference placeNameRef;
    ValueEventListener fromPhoneMessageListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            FirebaseMessage firebaseMessage = dataSnapshot.getValue(FirebaseMessage.class);
            System.out
                    .println("received from FireBase:\t" + firebaseMessage.getMessage());
            String message = firebaseMessage.getMessage();
                  /*if (!firebaseMessage.getBssid()
                        .equals(ConnectionState.getBssid(MainActivity.this))) */
            {
                System.out
                        .println("BSSIDs:\t" + firebaseMessage.getBssid() + "\t :"
                                + ConnectionState
                                .getBssid(MainActivity.this));
                TcpMessage tcpMessage = new Gson().fromJson(message, TcpMessage.class);
                onTcpMessageReceived(tcpMessage);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    private Ds3231 mDevice;

//        public static List<ROOM> getRooms(final Context ctx) {
//        db.roomDuo().deleteAll();
//        db.roomDuo().insert(new ROOM() {{
//            setId(117);
//            setControllerId(117);
//            setName("موقع 117");
//            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
//            setFloorId("1");
//        }});
//
//        db.roomDuo().insert(new ROOM() {{
//            setId(118);
//            setControllerId(118);
//            setName("موقع 118");
//            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
//            setFloorId("1");
//        }});
//
//        db.roomDuo().insert(new ROOM() {{
//            setId(119);
//            setControllerId(119);
//            setName("موقع 119");
//            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
//            setFloorId("1");
//        }});
//
//        db.roomDuo().insert(new ROOM() {{
//            setId(120);
//            setControllerId(120);
//            setName("موقع 120");
//            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
//            setFloorId("1");
//        }});
//
//        db.roomDuo().insert(new ROOM() {{
//            setId(121);
//            setControllerId(121);
//            setName("موقع 121");
//            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
//            setFloorId("1");
//        }});
//
//        db.roomDuo().insert(new ROOM() {{
//            setId(122);
//            setControllerId(122);
//            setName("موقع 122");
//            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
//            setFloorId("1");
//        }});
//
//        db.roomDuo().insert(new ROOM() {{
//            setId(123);
//            setControllerId(123);
//            setName("موقع 123");
//            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
//            setFloorId("1");
//        }});
//
//        db.roomDuo().insert(new ROOM() {{
//            setId(124);
//            setControllerId(124);
//            setName("موقع 124");
//            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
//            setFloorId("1");
//        }});
//
//        db.roomDuo().insert(new ROOM() {{
//            setId(125);
//            setControllerId(125);
//            setName("موقع 125");
//            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
//            setFloorId("1");
//        }});
//
//        db.roomDuo().insert(new ROOM() {{
//            setId(126);
//            setControllerId(126);
//            setName("موقع 126");
//            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
//            setFloorId("1");
//        }});
//
//        return db.roomDuo().ROOMS();
//    }
//
//    public static List<Device> getDevices(final Context ctx) {
//        db.deviceDuo().deleteAll();
//        for (int i = 117; i < 127; i++) {
//            final int finalI = i;
//            for (int j = 1; j < 8; j++) {
//                final int finalJ = j;
//                db.deviceDuo().insert(new Device() {{
//                    setId(Long
//                            .parseLong(finalI + String.format("%3d0", finalJ, Locale.US)));
//                    setDeviceId(finalJ);
//                    setControllerId(finalI);
//                    setName("إضاءه " + finalJ);
//                    setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
//                    setRoomId(finalI);
//                }});
//            }
//            db.deviceDuo().insert(new Device() {{
//                setId(Long.parseLong(finalI + String.format("%3d0", 20, Locale.US)));
//                setDeviceId(20);
//                setControllerId(finalI);
//                setName("مروحه 6");
//                setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
//                setRoomId(finalI);
//            }});
//        }
//
//        StringBuilder s = new StringBuilder("[");
//        for (Device device : db.deviceDuo().DEVICES()) {
//            s.append(new Gson().toJson(device, Device.class)).append(",");
//
//        }
//        s.append("]");
//        System.out.println("Devices ...  " + s);
//
//        return db.deviceDuo().DEVICES();
//    }


    private byte[] current = new byte[4];

    public static List<Floor> getFloors() {
        if (db.floorDuo().count() < 1) {
            db.floorDuo().insert(new Floor() {{
                setFloorId("f001");
                setName("الطابق الاول");
            }});
            db.floorDuo().insert(new Floor() {{
                setFloorId("f002");
                setName("الطابق الاول");
            }});
            db.floorDuo().insert(new Floor() {{
                setFloorId("f001");
                setName("الطابق الثاني");
            }});
            db.floorDuo().insert(new Floor() {{
                setFloorId("f003");
                setName("الطابق الثالث");
            }});
        }
        return db.floorDuo().FLOORS();
    }

    public static List<Room> getRooms(final Context ctx) {
        db.roomDuo().deleteAll();

        db.roomDuo().insert(new Room() {{
            setId(115);
            setControllerId(114);
            setName("موقع 115");
            setIconName(Utils.getDrawableNameById(
                    ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(116);
            setControllerId(116);
            setName("موقع 116");
            setIconName(Utils.getDrawableNameById(
                    ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(117);
            setControllerId(117);
            setName("موقع 117");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(118);
            setControllerId(118);
            setName("موقع 118");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(119);
            setControllerId(119);
            setName("موقع 119");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(120);
            setControllerId(120);
            setName("موقع 120");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(121);
            setControllerId(121);
            setName("موقع 121");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(122);
            setControllerId(122);
            setName("موقع 122");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(123);
            setControllerId(123);
            setName("موقع 123");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(124);
            setControllerId(124);
            setName("موقع 124");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(125);
            setControllerId(125);
            setName("موقع 125");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(126);
            setControllerId(126);
            setName("موقع 126");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        return db.roomDuo().ROOMS();
    }

    public static List<Room> getRoomsX(final Context ctx) {
        db.roomDuo().deleteAll();

        db.roomDuo().insert(new Room() {{
            setId(117);
            setControllerId(117);
            setName("الصاله الرئيسيه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(118);
            setControllerId(126);
            setName("صالة التدريب");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(119);
            setControllerId(126);
            setName("المخزن");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(120);
            setControllerId(126);
            setName("غرفة الاداره");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(121);
            setControllerId(126);
            setName("مغاسل الارضي");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(122);
            setControllerId(121);
            setName("الاستراحه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(123);
            setControllerId(114);
            setName("الممر");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(124);
            setControllerId(114);
            setName("مغاسل الاول");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(125);
            setControllerId(112);
            setName("Embedded System");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(126);
            setControllerId(120);
            setName("غرفة المبرمجين");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(127);
            setControllerId(115);
            setName("صالة المبرمجين");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        return db.roomDuo().ROOMS();
    }

    public static List<Device> getDevices(final Context ctx) {
        db.deviceDuo().deleteAll();

        db.deviceDuo().insert(new Device() {{
            setId(115001);
            setDeviceId(1);
            setControllerId(115);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(115);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115002);
            setDeviceId(2);
            setControllerId(115);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(115);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115003);
            setDeviceId(3);
            setControllerId(115);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(115);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115004);
            setDeviceId(4);
            setControllerId(115);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(115);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115005);
            setDeviceId(5);
            setControllerId(115);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(115);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115006);
            setDeviceId(6);
            setControllerId(115);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(115);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(117006);
            setDeviceId(6);
            setControllerId(117);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117001);
            setDeviceId(1);
            setControllerId(117);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117002);
            setDeviceId(2);
            setControllerId(117);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117003);
            setDeviceId(3);
            setControllerId(117);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117004);
            setDeviceId(4);
            setControllerId(117);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117005);
            setDeviceId(5);
            setControllerId(117);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117006);
            setDeviceId(6);
            setControllerId(117);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(118001);
            setDeviceId(1);
            setControllerId(118);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118002);
            setDeviceId(2);
            setControllerId(118);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118003);
            setDeviceId(3);
            setControllerId(118);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118004);
            setDeviceId(4);
            setControllerId(118);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118005);
            setDeviceId(5);
            setControllerId(118);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118006);
            setDeviceId(6);
            setControllerId(118);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(119001);
            setDeviceId(1);
            setControllerId(119);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119002);
            setDeviceId(2);
            setControllerId(119);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119003);
            setDeviceId(3);
            setControllerId(119);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119004);
            setDeviceId(4);
            setControllerId(119);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119005);
            setDeviceId(5);
            setControllerId(119);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119006);
            setDeviceId(6);
            setControllerId(119);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(120001);
            setDeviceId(1);
            setControllerId(120);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120002);
            setDeviceId(2);
            setControllerId(120);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120003);
            setDeviceId(3);
            setControllerId(120);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120004);
            setDeviceId(4);
            setControllerId(120);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120005);
            setDeviceId(5);
            setControllerId(120);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120006);
            setDeviceId(6);
            setControllerId(120);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(121001);
            setDeviceId(1);
            setControllerId(121);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(121002);
            setDeviceId(2);
            setControllerId(121);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(121003);
            setDeviceId(3);
            setControllerId(121);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(121004);
            setDeviceId(4);
            setControllerId(121);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(121005);
            setDeviceId(5);
            setControllerId(121);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(121006);
            setDeviceId(6);
            setControllerId(121);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(122001);
            setDeviceId(1);
            setControllerId(122);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(122002);
            setDeviceId(2);
            setControllerId(122);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(122003);
            setDeviceId(3);
            setControllerId(122);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(122004);
            setDeviceId(4);
            setControllerId(122);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(122005);
            setDeviceId(5);
            setControllerId(122);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(122006);
            setDeviceId(6);
            setControllerId(122);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(123001);
            setDeviceId(1);
            setControllerId(123);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(123002);
            setDeviceId(2);
            setControllerId(123);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(123003);
            setDeviceId(3);
            setControllerId(123);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(123004);
            setDeviceId(4);
            setControllerId(123);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(123005);
            setDeviceId(5);
            setControllerId(123);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(123006);
            setDeviceId(6);
            setControllerId(123);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(119001);
            setDeviceId(1);
            setControllerId(119);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119002);
            setDeviceId(2);
            setControllerId(119);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119003);
            setDeviceId(3);
            setControllerId(119);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119004);
            setDeviceId(4);
            setControllerId(119);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119005);
            setDeviceId(5);
            setControllerId(119);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119006);
            setDeviceId(6);
            setControllerId(119);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(125001);
            setDeviceId(1);
            setControllerId(125);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(125002);
            setDeviceId(2);
            setControllerId(125);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(125003);
            setDeviceId(3);
            setControllerId(125);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(125004);
            setDeviceId(4);
            setControllerId(125);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(125005);
            setDeviceId(5);
            setControllerId(125);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(125006);
            setDeviceId(6);
            setControllerId(125);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(126001);
            setDeviceId(1);
            setControllerId(126);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126002);
            setDeviceId(2);
            setControllerId(126);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126003);
            setDeviceId(3);
            setControllerId(126);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126004);
            setDeviceId(4);
            setControllerId(126);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126005);
            setDeviceId(5);
            setControllerId(126);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126006);
            setDeviceId(6);
            setControllerId(126);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(116001);
            setDeviceId(1);
            setControllerId(116);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(116);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(116002);
            setDeviceId(2);
            setControllerId(116);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(116);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(116003);
            setDeviceId(3);
            setControllerId(116);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(116);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(116004);
            setDeviceId(4);
            setControllerId(116);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(116);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(116005);
            setDeviceId(5);
            setControllerId(116);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(116);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(116006);
            setDeviceId(6);
            setControllerId(116);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(116);
        }});

        StringBuilder s = new StringBuilder("[");
        for (Device device : db.deviceDuo().DEVICES()) {
            s.append(new Gson().toJson(device, Device.class)).append(",");

        }
        s.append("]");
        System.out.println("Devices ...  " + s);

        return db.deviceDuo().DEVICES();
    }

    public static List<Device> getDevicesX(final Context ctx) {
        db.deviceDuo().deleteAll();

        //region room 117
        db.deviceDuo().insert(new Device() {{
            setId(118003);
            setDeviceId(3);
            setControllerId(118);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118004);
            setDeviceId(4);
            setControllerId(118);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118002);
            setDeviceId(2);
            setControllerId(118);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118005);
            setDeviceId(5);
            setControllerId(118);
            setName("بلك سويج 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118006);
            setDeviceId(6);
            setControllerId(118);
            setName("بلك سويج 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117003);
            setDeviceId(3);
            setControllerId(117);
            setName("المعرض");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117006);
            setDeviceId(6);
            setControllerId(117);
            setName("بلك سويج 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        //endregion
        //region room 118
        db.deviceDuo().insert(new Device() {{
            setId(126002);
            setDeviceId(2);
            setControllerId(126);
            setName("إضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126006);
            setDeviceId(6);
            setControllerId(126);
            setName("بلك سويج");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        //endregion
        //region room 119
        db.deviceDuo().insert(new Device() {{
            setId(119004);
            setDeviceId(4);
            setControllerId(126);
            setName("إضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});
        //endregion
        //region room 120
        db.deviceDuo().insert(new Device() {{
            setId(119003);
            setDeviceId(3);
            setControllerId(119);
            setName("إضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126006);
            setDeviceId(6);
            setControllerId(126);
            setName("بلك سويج");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        //endregion
        //region room 121
        db.deviceDuo().insert(new Device() {{
            setId(126005);
            setDeviceId(5);
            setControllerId(126);
            setName("إضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});

        //endregion

        //region room 122
        db.deviceDuo().insert(new Device() {{
            setId(121005);
            setDeviceId(5);
            setControllerId(121);
            setName("إضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(121006);
            setDeviceId(6);
            setControllerId(121);
            setName("البصمه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});

        //endregion
        //region room 123
        db.deviceDuo().insert(new Device() {{
            setId(114004);
            setDeviceId(4);
            setControllerId(114);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(114005);
            setDeviceId(5);
            setControllerId(114);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115006);
            setDeviceId(6);
            setControllerId(115);
            setName("بلك سويج");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});

        //endregion
        //region room 124
        db.deviceDuo().insert(new Device() {{
            setId(114003);
            setDeviceId(3);
            setControllerId(114);
            setName("إضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});
        //endregion
        //region room 125
        db.deviceDuo().insert(new Device() {{
            setId(112003);
            setDeviceId(3);
            setControllerId(112);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(112004);
            setDeviceId(4);
            setControllerId(112);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(112006);
            setDeviceId(6);
            setControllerId(112);
            setName("بلك سويج");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        //endregion
        //region room 126
        db.deviceDuo().insert(new Device() {{
            setId(120002);
            setDeviceId(2);
            setControllerId(120);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120003);
            setDeviceId(3);
            setControllerId(120);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120005);
            setDeviceId(5);
            setControllerId(120);
            setName("بلك سويج يمين");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120006);
            setDeviceId(6);
            setControllerId(120);
            setName("بلك سويج يسار");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});

        //endregion
        //region room 127
        db.deviceDuo().insert(new Device() {{
            setId(115002);
            setDeviceId(2);
            setControllerId(115);
            setName("اضاءه خارجيه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(127);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115003);
            setDeviceId(3);
            setControllerId(115);
            setName("اضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(127);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115005);
            setDeviceId(5);
            setControllerId(115);
            setName("بلك سويج");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(127);
        }});

        //endregion

        return db.deviceDuo().DEVICES();
    }

    public static List<String> GetFloorsIds(List<Room> rooms) {
        List<String> ids = new ArrayList<>();
        for (Room room : rooms) {
            if (!ids.contains(room.getFloorId())) {
                ids.add(room.getFloorId());
            }
        }
        return ids;
    }

    public static void refreshDevices() {
//            seekRefresh.setVisibility(View.VISIBLE);
//            for (int i = 100; i < 165; i++) {
//                  seekRefresh.setProgress(i);
//                  try {
//                        final int finalI = i;
//                        UARTServer.send(
//                              new UARTMessage() {{
//                                    setControllerAddress(finalI);
//                                    setDeviceAddress(0);
//                                    setCmd("G");
//                                    setState(0);
//                              }}
//                        );
//                        Thread.sleep(300);
//                  } catch (InterruptedException e) {
//                        e.printStackTrace();
//                  }
//            }
        UARTMessage uartMessage = new UARTMessage();
        uartMessage.setControllerAddress(50);
        uartMessage.setDeviceAddress(0);
        uartMessage.setCmd("G");
        uartMessage.setState(0);

        UARTServer.send(uartMessage);
    }

    public static float getFloat32(byte[] bytes) {
        return Float.intBitsToFloat(
                ((bytes[3] & 0xFF) << 24)
                        | ((bytes[2] & 0xFF) << 16)
                        | ((bytes[1] & 0xFF) << 8)
                        | ((bytes[0] & 0xFF)));
    }

    private void initX(boolean x) {
        if (x) {
            if (db.roomDuo().count() < 1 || true) {
                getRoomsX(MainActivity.this);
            }
            if (db.deviceDuo().count() < 1 || true) {
                getDevicesX(MainActivity.this);
            }
        } else {
            if (db.roomDuo().count() < 1 || true) {
                getRooms(MainActivity.this);
            }
            if (db.deviceDuo().count() < 1 || true) {
                getDevices(MainActivity.this);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int s = LocalData.getInstance(this).getInt(OTHER.RESET);
        LocalData.getInstance(this).add(OTHER.RESET, 0);

        instance = this;


        mNSDListener = new NSDListen(this);

        mNSDListener.registerDevice();

        setTitle("SmartHome");
//            IPS.add("192.168.0.116"); //Ali's ip
//            WRC();
        seekRefresh = findViewById(R.id.seekRefresh);

        txtTcpMessages = findViewById(R.id.txtTcpMessages);
        scrollTcpMessages = findViewById(R.id.scrollTcpMessages);
        txtUartMessages = findViewById(R.id.txtUartMessages);
        scrollUartMessages = findViewById(R.id.scrollUartMessages);

        txtTcpMessagesOut = findViewById(R.id.txtTcpMessagesOut);
        scrollTcpMessagesOut = findViewById(R.id.scrollTcpMessagesOut);
        txtUartMessagesOut = findViewById(R.id.txtUartMessagesOut);
        scrollUartMessagesOut = findViewById(R.id.scrollUartMessagesOut);

        txtTcpMessages.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                scrollTcpMessages.scrollTo(0, 0);
            }
        });
        txtUartMessages.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                scrollUartMessages.scrollTo(0, 0);
            }
        });

        ctx = MainActivity.this;
        seekRefresh.setProgress(80);
        SV.DEVICE_ID = SV.GET_DEVICE_ID(MainActivity.this);
        db = android.arch.persistence.room.Room.databaseBuilder(
                MainActivity.this, DB.class,
                "smartHome.db"
        )
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
        getSunriseSunset();
        initX(true);

        db.relationDuo().deleteAll();
//        relationTst();
        firebaseReference = database.getReference(SV.GET_DEVICE_ID(getApplicationContext()));
        fromControllerMessageRef = firebaseReference.child("fromControllerMessage");
        fromPhoneMessageRef = firebaseReference.child("fromPhoneMessageRef");
//            fromPhoneMessageRef.addValueEventListener(fromPhoneMessageListener);
        placeNameRef = firebaseReference.child("name");
        devicesRef = firebaseReference.child("devices");
        roomsRef = firebaseReference.child("rooms");
        roomsRef.setValue(null);
        devicesRef.setValue(null);

        placeNameRef.setValue("AlMorabaa", new CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError,
                                   DatabaseReference databaseReference) {
                if (databaseError != null) {
                    System.out.println("Firebase : \t " + databaseError.getDetails());
                }
            }
        });

//            Intent uartServiceIntent = new Intent(
//                  MainActivity.this,
//                  UARTServer.class);
//            
//            startService(uartServiceIntent);

        UARTServer uartServer = new UARTServer();
        uartServer.setUartCallback(this);

        Intent tcpServiceIntent = new Intent(
                MainActivity.this,
                TcpServerService.class);

        startService(tcpServiceIntent);

//            TcpServerService.getInstant().setTcpCallback(this);

//            UARTServer.getInstant().setUartCallback(this);

//            new Thread(new Runnable() {
//                  @Override
//                  public void run() {
//                        try {
//                              Thread.sleep(1500);
//                              for (int i = 100; i <= 127; i++) {
//                                    UARTMessage uartMessage =
//                                          new UARTMessage();
//                                    uartMessage.setControllerAddress(
//                                          i);
//                                    uartMessage.setDeviceAddress(0);
//                                    uartMessage.setCmd("S");
//                                    uartMessage.setState(0);
//                                    UARTServer.send(uartMessage);
//                              }
//                        } catch (InterruptedException e) {
//                        }
//                  }
//            }).start();
    }

    private void relationTst() {

        System.out.println("Relations: " + addRelation(new Relation() {{
            setDeviceId(126002);
            setToConnectDeviceId(126001);
            setRelationType(TYPE.WITH);
        }}));
        System.out.println("Relations: " + addRelation(new Relation() {{
            setDeviceId(126003);
            setToConnectDeviceId(126001);
            setRelationType(TYPE.WITH);
        }}));
        System.out.println("Relations: " + addRelation(new Relation() {{
            setDeviceId(126001);
            setToConnectDeviceId(126002);
            setRelationType(TYPE.WITH);
        }}));
        System.out.println("Relations: " + addRelation(new Relation() {{
            setDeviceId(126003);
            setToConnectDeviceId(126002);
            setRelationType(TYPE.WITH);
        }}));
        System.out.println("Relations: " + addRelation(new Relation() {{
            setDeviceId(126001);
            setToConnectDeviceId(126003);
            setRelationType(TYPE.WITH);
        }}));
        System.out.println("Relations: " + addRelation(new Relation() {{
            setDeviceId(126002);
            setToConnectDeviceId(126003);
            setRelationType(TYPE.WITH);
        }}));
    }

    void saveDevice(final Device receivedDevice) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Long> devicesIds = new ArrayList<>();
                for (Device device : db.deviceDuo().DEVICES()) {
                    devicesIds.add(device.getId());
                }

                if (!devicesIds.contains(receivedDevice.getId())) {
                    receivedDevice
                            .setName("جهاز جديد " + receivedDevice.getDeviceId());

                    db.deviceDuo().insert(receivedDevice);
                    System.out.println("Device  added" +
                            new Gson().toJson(receivedDevice, Device.class)
                    );
                }

                StringBuilder s = new StringBuilder("[");
                for (Device device : db.deviceDuo().DEVICES()) {
                    s.append(new Gson().toJson(device, Device.class)).append(",");

                }
                s.append("]");
//                        System.out.println("Devices ...  " + s);
            }
        }).start();
    }

    public void showQRCode(View view) {
        Intent uartServiceIntent = new Intent(
                MainActivity.this, UARTServer.class);
        startService(uartServiceIntent);
//            Intent intent = new Intent(MainActivity.this, QRActivity.class);
//            startActivity(intent);
    }

    void getSunriseSunset() {
        Location location = new Location("32.019065", "-44.3520314");
        SunriseSunsetCalculator calculator = new SunriseSunsetCalculator(location,
                "GMT-0300");

        String officialSunrise = calculator.getOfficialSunriseForDate(Calendar.getInstance());
        String officialSunset = calculator.getOfficialSunsetForDate(Calendar.getInstance());
        System.out.println("officialSunrise:\t" + officialSunrise + "\n" +
                "officialSunset:\t" + officialSunset);
    }

    void setConfigs() {
        WifiManager wifiManager = (WifiManager) getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);
        WifiConfiguration wifiConf = getCurrentWiFiConfiguration(getApplicationContext());

        try {
            setStaticIpConfiguration(wifiManager, wifiConf,
                    InetAddress.getByName("192.168.0.109"),
                    24,
                    InetAddress.getByName("10.0.0.2"),
                    new InetAddress[]{InetAddress.getByName("10.0.0.3"),
                            InetAddress.getByName("10.0.0.4")});

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void WRC() {
        Date date;
        try {
            mDevice = new Ds3231(BoardDefaults.getI2CPort());
            Log.d(TAG, "isTimekeepingDataValid = "
                    + mDevice.isTimekeepingDataValid());
            Log.d(TAG, "isOscillatorEnabled = "
                    + mDevice.isOscillatorEnabled());

            Calendar calendar = Calendar.getInstance();
            calendar.set(1982, Calendar.DECEMBER, 22);

            date = calendar.getTime();

//                  Log.d(TAG, "DateTime = " + date.toString());
//                  mDevice.setTime(date);
//                  Log.d(TAG, "getTime = " + mDevice.getTime().toString());
//                  mDevice.setTime(date.getTime());
//                  Log.d(TAG, "getTime = " + mDevice.getTime().toString());
//
            Log.d(TAG, "mDevice = " + mDevice.readTemperature());

            date = new Date(System.currentTimeMillis());
            Log.d(TAG, "DateTime = " + date.toString());
            mDevice.setTime(date);
            Log.d(TAG, "getDeviceTime = " + mDevice.getTime().toString());
            mDevice.setTime(date.getTime());
            Log.d(TAG, "getDeviceTime = " + mDevice.getTime().toString());
            // Close the device.
            mDevice.close();
        } catch (IOException e) {
            Log.e(TAG, "Error while opening screen", e);

        } finally {
            mDevice = null;
        }
    }

    @Override
    public void onTcpMessageReceived(TcpMessage tcpMessage) {
        if (tcpMessage.getDeviceId().equals(SV.DEVICE_ID) || true) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txtTcpMessages.setText(tcpMessage.toString() + "\n" +
                            txtTcpMessages.getText()
                    );
                }
            });

            switch (tcpMessage.getBodyClass()) {
                case "Device": {
                    String deviceString = tcpMessage.getBody();
                    final Device device = new Gson()
                            .fromJson(deviceString, Device.class);
                    onDeviceReceived(device, tcpMessage.getCommand());

                    break;
                }
                case "Room": {
                    final Room room = new Gson()
                            .fromJson(tcpMessage.getBody(), Room.class);
                    onRoomReceived(room, tcpMessage.getCommand());
                    break;
                }
                case "Other": {
                    if (tcpMessage.getCommand().equals(OTHER.RESET)) {
                        reset();
                    }
                    break;
                }
            }
        }
    }

    private void reset() {
        LocalData.getInstance(this).add(OTHER.RESET, 1);
        new Handler(Looper.getMainLooper())
                .postDelayed(() -> System.exit(0), 10000);
    }

    private void onRoomReceived(Room room, String command) {
        if (command.equals(ROOM.GET_ROOM_DEVICES)) {
            UARTMessage uartMessage =
                    new UARTMessage();
            uartMessage.setCmd("G");
            uartMessage.setState(0);
            List<Device> devices = db.deviceDuo()
                    .getDevicesByRoomId(room.getId());
            for (Device device : devices) {
                uartMessage.setControllerAddress(
                        device.getControllerId());
                uartMessage.setDeviceAddress(device.getDeviceId());
                UARTServer.send(uartMessage);
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            {// get sensors
                uartMessage.setControllerAddress(
                        room.getControllerId());

                uartMessage.setDeviceAddress(30);
                UARTServer.send(uartMessage);

                uartMessage.setDeviceAddress(31);
                UARTServer.send(uartMessage);

                uartMessage.setDeviceAddress(32);
                UARTServer.send(uartMessage);
            }
        }
    }

    private void onDeviceReceived(Device device, String command) {
        switch (command) {
            case SV.TCP_COMMANDS.DEVICE.SET: {
                UARTMessage uartMessage =
                        new UARTMessage();
                uartMessage.setControllerAddress(
                        device.getControllerId());
                uartMessage.setDeviceAddress(device.getDeviceId());
                uartMessage.setCmd("S");
                uartMessage.setState(device.getStatus());
                UARTServer.send(uartMessage);
                break;
            }
            case SV.TCP_COMMANDS.DEVICE.GET: {
                UARTMessage uartMessage =
                        new UARTMessage() {{
                            setControllerAddress(
                                    device.getControllerId());
                            setDeviceAddress(device.getDeviceId());
                            setCmd("G");
                            setState(0);
                        }};

                UARTServer.send(uartMessage);
                break;
            }
            case SV.TCP_COMMANDS.DEVICE.GET_ALL: {
                for (Device d : db.deviceDuo().DEVICES()) {
                    TcpMessage tcpMessageSend = new TcpMessage();
                    tcpMessageSend
                            .setCommand(SV.TCP_COMMANDS.DEVICE.UPDATE);
                    tcpMessageSend
                            .setBody(new Gson().toJson(d, Device.class));
                    tcpMessageSend.setBodyClass("Device");
                    TcpClient.send(tcpMessageSend);
                }
                break;
            }
            case DEVICE.TURN_ALL_ON: {
                for (Device d : db.deviceDuo().DEVICES()) {
                    UARTMessage uartMessage =
                            new UARTMessage();
                    uartMessage.setControllerAddress(
                            d.getControllerId());
                    uartMessage.setDeviceAddress(d.getDeviceId());
                    uartMessage.setCmd("S");
                    uartMessage.setState(1);
                    UARTServer.send(uartMessage);
                }
                break;
            }
            case DEVICE.TURN_ALL_OFF: {
                for (Device d : db.deviceDuo().DEVICES()) {
                    if (d.getId() != 126006) {
                        UARTMessage uartMessage =
                                new UARTMessage();
                        uartMessage.setControllerAddress(
                                d.getControllerId());
                        uartMessage.setDeviceAddress(d.getDeviceId());
                        uartMessage.setCmd("S");
                        uartMessage.setState(0);
                        UARTServer.send(uartMessage);
                    }
                }
                break;
            }
            case SV.TCP_COMMANDS.OTHER.REFRESH: {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        refreshDevices();
                    }
                }).start();
                break;
            }
            case SV.TCP_COMMANDS.DEVICE.UPDATE: {
                db.deviceDuo().Update(device);
//                                          Device currentDevice = db.deviceDuo()
//                                                .getDeviceByID(device.getId());

                TcpMessage message1 = new TcpMessage();
                message1.setCommand(SV.TCP_COMMANDS.DEVICE.UPDATE);
                message1.setBody(new Gson().toJson(device, Device.class));
                System.out.println("updated:" +
                        "\n" + new Gson()
                        .toJson(message1, TcpMessage.class));
                TcpClient.send(message1);
                devicesRef.setValue(null);
                for (Device deviceX : db.deviceDuo().DEVICES()) {
                    devicesRef.push().setValue(deviceX);
                }
                break;
            }
        }
    }

    private void getRelatives(Device device) {
        List<Relation> relations = db.relationDuo().getRelationsByDeviceId(device.getId());
        for (Relation relation : relations) {
            Device toConnectDevice = db.deviceDuo()
                    .getDeviceById(relation.getToConnectDeviceId());
            if (relation.getRelationType() == Relation.TYPE.WITH) {
                UARTMessage uartMessage = new UARTMessage();
                uartMessage.setControllerAddress(toConnectDevice.getControllerId());
                uartMessage.setDeviceAddress(toConnectDevice.getDeviceId());
                uartMessage.setCmd("S");
                uartMessage.setState(device.getStatus());
                UARTServer.send(uartMessage);
            } else if (relation.getRelationType() == Relation.TYPE.OPPOSITE) {
                UARTMessage uartMessage = new UARTMessage();
                uartMessage.setControllerAddress(toConnectDevice.getControllerId());
                uartMessage.setDeviceAddress(toConnectDevice.getDeviceId());
                uartMessage.setCmd("S");
                uartMessage.setState(device.getStatus() == 0 ? 1 : 0);
                UARTServer.send(uartMessage);
            }
            System.out.println("relation: " +
                    relation.getDeviceId() + "\t" +
                    relation.getRelationType() + "\t" +
                    relation.getToConnectDeviceId()
            );
        }
    }

    @Override
    public void onUartMessageReceived(UARTMessage uartMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txtUartMessages.setText(uartMessage.toString() + "\n" +
                        txtUartMessages.getText());
            }
        });
        final Device receivedDevice = new Device();
        TcpMessage tcpMessage = new TcpMessage();
        try {
            receivedDevice.setId(Long.parseLong(
                    uartMessage.getControllerAddress() + "" +
                            String.format(Locale.US, "%03d",
                                    uartMessage.getDeviceAddress())
            ));

            receivedDevice.setDeviceId((int) uartMessage.getDeviceAddress());
            receivedDevice.setStatus(uartMessage.getState());
            receivedDevice
                    .setControllerId((int) uartMessage.getControllerAddress());
            receivedDevice
                    .setRoomId((int) uartMessage.getControllerAddress());
            try {
                receivedDevice
                        .setAmperes(Integer.parseInt(uartMessage.getCmd()));
            } catch (Exception e) {
                receivedDevice
                        .setAmperes(1.2f);
            }
            if (receivedDevice.getDeviceId() > 0 && receivedDevice.getDeviceId() < 20) {
                receivedDevice.setType("single");
            } else if (receivedDevice.getDeviceId() < 30) {
                receivedDevice.setType("multiValue");
            } else if (receivedDevice.getDeviceId() > 29) {
                receivedDevice.setType("sensor");
            }

//            getRelatives(receivedDevice);

            tcpMessage.setCommand(SV.TCP_COMMANDS.DEVICE.SET);

            tcpMessage.setBody(receivedDevice.minify());

            TcpClient.send(tcpMessage);
        } catch (NumberFormatException ignored) {
        }
//        FirebaseMessage.send(message1);
//            saveDevice(receivedDevice);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onUartBufferReceived(byte... uartBuffer) {
        runOnUiThread(() ->
                txtUartMessages.setText(
                        fromSigned(uartBuffer[0]) + "," +
                                fromSigned(uartBuffer[1]) + "," +
                                fromSigned(uartBuffer[2]) + "\n" +
                                txtUartMessages.getText()
                ));
        TcpMessage tcpMessage = new TcpMessage();
        tcpMessage.setCommand(TCP_COMMANDS.OTHER.DEBUG);
        tcpMessage.setBody(
                fromSigned(uartBuffer[0]) + "," +
                        fromSigned(uartBuffer[1]) + "," +
                        fromSigned(uartBuffer[2])
        );
        System.out.println("Debug buffer: " + tcpMessage.getBody());
        TcpClient.send(tcpMessage);

        if (uartBuffer[0] == 18) {
            current[0] = uartBuffer[1];
            current[1] = uartBuffer[2];
        } else if (uartBuffer[0] == 19) {
            current[2] = uartBuffer[1];
            current[3] = uartBuffer[2];
            tcpMessage.setBody(getFloat32(current) + "");
            System.out.println("Debug buffer: " + tcpMessage.getBody());
            TcpClient.send(tcpMessage);
            System.out.println(
                    "current: " + getFloat32(current)
            );
        }


    }

    private int fromSigned(byte b) {
        return (int) b < 0 ? (int) b + 256 : (int) b;
    }

    public String cycleTest(List<Map.Entry<Long, Long>> points) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
        long vertexCount = 10;
        for (Map.Entry<Long, Long> point : points) {
            if (point.getKey() > vertexCount) {
                vertexCount = point.getKey();
            }
            if (point.getValue() > vertexCount) {
                vertexCount = point.getKey();
            }
        }
        List<Long>[] adj_data = (List<Long>[]) new ArrayList[(int) vertexCount + 1];
        long from;
        long to;
        for (Map.Entry<Long, Long> point : points) {
            from = point.getKey();
            to = point.getValue();
            if (adj_data[(int) from] == null) {
                adj_data[(int) from] = new ArrayList<>();
            }
            adj_data[(int) from].add(to);
        }
        return new CycleDetector(adj_data, adj_data.length).findCycleForVertex();
    }

    public boolean addRelation(Relation relation) {
        List<Relation> relations = db.relationDuo().RELATIONS();
        List<Map.Entry<Long, Long>> points = new ArrayList<>();

        for (Relation r : relations) {
            System.out.println("Point: " + r.getDeviceId() + "\t" + r.getToConnectDeviceId());
            points.add(
                    new AbstractMap.SimpleEntry<>(
                            r.getDeviceId(),
                            r.getToConnectDeviceId()
                    )
            );
        }
        String result = cycleTest(points);

        System.out.println(result);
        if (result.contains("no cycles found for vertex")) {
            db.relationDuo().insert(relation);
            return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            fGpio.setValue(false);
            mGpio.setValue(false);
        } catch (IOException e) {
        }
    }
}

